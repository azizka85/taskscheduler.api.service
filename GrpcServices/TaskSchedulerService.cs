﻿using System;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using TaskScheduler.API.Service.Services;
using TaskScheduler.Extensions;
using TaskScheduler.Data;
using TaskScheduler.Services.TaskScheduler;
using TaskSchedulerServiceGrpc = TaskScheduler.Services.TaskScheduler.TaskSchedulerService.TaskSchedulerServiceBase;

namespace TaskScheduler.API.Service.GrpcServices
{
    public class TaskSchedulerService : TaskSchedulerServiceGrpc
    {
        private readonly TasksService _tasksService;
        
        private readonly TaskStreamsManager _taskStreamsManager;

        private readonly ILogger<TaskSchedulerService> _logger;

        public TaskSchedulerService(
            TasksService tasksService, 
            TaskStreamsManager taskStreamsManager, 
            ILogger<TaskSchedulerService> logger)
        {
            _tasksService = tasksService ?? throw new ArgumentNullException(nameof(tasksService));
            
            _taskStreamsManager = taskStreamsManager ??
                                           throw new ArgumentNullException(nameof(taskStreamsManager));

            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public override async Task Call(Empty request, IServerStreamWriter<CallResponse> responseStream, ServerCallContext context)
        {
            var streamId = Guid.NewGuid().ToString();
            var result = _taskStreamsManager.RegisterStream(streamId, responseStream);

            if (result)
            {
                try
                {
                    await context.CancellationToken;
                }
                catch (Exception exception)
                {
                    _logger.LogWarning(exception, "Error on connecting to TaskSchedulerService for stream={StreamId}", streamId);
                }
            }

            _taskStreamsManager.UnRegisterStream(streamId);
        }

        public override async Task<AddResponse> Add(TaskItem request, ServerCallContext context)
        {
            return new AddResponse
            {
                Success = await _tasksService.AddAsync(request)
            };
        }
    }
}