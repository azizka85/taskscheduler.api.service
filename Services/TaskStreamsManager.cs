﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using TaskScheduler.Services.TaskScheduler;
using TaskScheduler.Data;

namespace TaskScheduler.API.Service.Services
{
    public class TaskStreamsManager : IRedisNotifier
    {
        private readonly ConcurrentDictionary<string, IServerStreamWriter<CallResponse>> _streams;
        
        private readonly IConnectionMultiplexer _connectionMultiplexer;

        private readonly ILogger<TaskStreamsManager> _logger;

        public TaskStreamsManager(
            IConnectionMultiplexer connectionMultiplexer,
            ILogger<TaskStreamsManager> logger)
        {
            _streams = new ConcurrentDictionary<string, IServerStreamWriter<CallResponse>>();
            
            _connectionMultiplexer = connectionMultiplexer ?? throw new ArgumentNullException(nameof(connectionMultiplexer));

            _logger = logger ?? throw new ArgumentNullException();
        }

        public bool RegisterStream(string streamKey, IServerStreamWriter<CallResponse> responseStream)
        {
            return _streams.TryAdd(streamKey, responseStream);
        }

        public bool UnRegisterStream(string streamKey)
        {
            return _streams.TryRemove(streamKey, out _);
        }
        
        public async Task NotifyAsync(string? key, RedisNotificationTypes? notificationType)
        {
            try
            {
                if (key != null && key.StartsWith("task_"))
                {
                    await SendAsync(key, notificationType);
                }
            }
            catch (Exception exception)
            {
                _logger.LogWarning(exception, "Error when notifying redis event {Name} for key {Key}", notificationType?.GetName(), key);
            }
        }

        public async Task SendAsync(string key, RedisNotificationTypes? notificationType)
        {
            switch (notificationType)
            {
                case RedisNotificationTypes.Expire:
                    var db = _connectionMultiplexer.GetDatabase();
                    var task = TaskItem.Parser.ParseFrom(db.StringGet(key));
                    await SendCreateMessageAsync(task);
                    break;
                case RedisNotificationTypes.Expired:
                    var id = key.Substring("task_".Length);
                    await SendDeleteMessageAsync(id);
                    break;
                default:
                    _logger.LogWarning("Unhandled notificationType: {Name}", notificationType?.GetName());
                    break;
            }
        }

        public async Task SendCreateMessageAsync(TaskItem task)
        {
            await SendMessageAsync(
                new CallResponse
                {
                    Create = new CreateMessage
                    {
                        Task = task
                    }
                });
        }

        public async Task SendDeleteMessageAsync(string id)
        {
            await SendMessageAsync(
                new CallResponse
                {
                    Delete = new DeleteMessage
                    {
                        TaskId = id
                    }
                });
        }
        
        public async Task SendMessageAsync(CallResponse response)
        {
            foreach (var stream in _streams)
            {
                try
                {
                    await stream.Value.WriteAsync(response);
                }
                catch (Exception exception)
                {
                    _logger.LogWarning(exception, "Error on writing to stream of TaskSchedulerService for stream={Key}", stream.Key);
                }
            }
        }
    }
}