﻿using System.Threading.Tasks;
using StackExchange.Redis;

namespace TaskScheduler.API.Service.Services
{
    public interface IRedisNotifier
    {
        Task NotifyAsync(string? key, RedisNotificationTypes? notificationType);
    }
}