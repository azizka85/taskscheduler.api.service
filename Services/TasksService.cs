﻿using System;
using System.IO;
using System.Threading.Tasks;
using Google.Protobuf;
using StackExchange.Redis;
using TaskScheduler.Data;

namespace TaskScheduler.API.Service.Services
{
    public class TasksService
    {
        private readonly IConnectionMultiplexer _connectionMultiplexer;

        public TasksService(IConnectionMultiplexer connectionMultiplexer)
        {
            _connectionMultiplexer = connectionMultiplexer ?? throw new ArgumentNullException(nameof(connectionMultiplexer));
        }

        public Task<bool> AddAsync(TaskItem task)
        {
            var db = _connectionMultiplexer.GetDatabase();

            var id = Guid.NewGuid().ToString();

            task.Id = id;
            
            Console.WriteLine($"Start: {task.Start.ToDateTime()}, stop: {task.Stop.ToDateTime()}");

            var data = GetBytes(task);

            return db.StringSetAsync(
                $"task_{id}", 
                data, 
                task.Stop.ToDateTime() - task.Start.ToDateTime());
        }

        public byte[] GetBytes(TaskItem task)
        {
            using MemoryStream stream = new MemoryStream();
            
            task.WriteTo(stream);
                
            return stream.ToArray();
        }
    }
}