﻿using System;

namespace TaskScheduler.API.Service.Services
{
    public enum RedisNotificationTypes
    {
        Expire,
        Expired,
        RenameFrom,
        RenameTo,
        Del,
        Evicted,
        Set
    }

    public static class RedisNotificationTypesExtensions
    {
        public static RedisNotificationTypes? Get(string? notificationType)
        {
            return notificationType switch
            {
                "expire" => RedisNotificationTypes.Expire,
                "expired" => RedisNotificationTypes.Expired,
                "rename_from" => RedisNotificationTypes.RenameFrom,
                "rename_to" => RedisNotificationTypes.RenameTo,
                "del" => RedisNotificationTypes.Del,
                "evicted" => RedisNotificationTypes.Evicted,
                "set" => RedisNotificationTypes.Set,
                _ => null
            };
        }

        public static string? GetName(this RedisNotificationTypes notificationType)
        {
            return notificationType switch
            {
                RedisNotificationTypes.Expire => "expire",
                RedisNotificationTypes.Expired => "expired",
                RedisNotificationTypes.RenameFrom => "rename_from",
                RedisNotificationTypes.RenameTo => "rename_to",
                RedisNotificationTypes.Del => "del",
                RedisNotificationTypes.Evicted => "evicted",
                RedisNotificationTypes.Set => "set",
                _ => null
            };
        }
    }
}