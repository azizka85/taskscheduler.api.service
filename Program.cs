using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace TaskScheduler.API.Service
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            WebHost
                .CreateDefaultBuilder(args)
                .SuppressStatusMessages(true)
                .ConfigureAppConfiguration((_, config) =>
                    config
                        .AddEnvironmentVariables())
                .UseStartup<Startup>()
                .Build()
                .Run();
        }
    }
}