﻿FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["taskscheduler/TaskScheduler.csproj", "taskscheduler/"]
COPY ["taskscheduler.api.service/TaskScheduler.API.Service.csproj", "taskscheduler.api.service/"]
RUN dotnet restore "taskscheduler.api.service/TaskScheduler.API.Service.csproj"
COPY . .
WORKDIR "/src/taskscheduler.api.service"
RUN dotnet build "TaskScheduler.API.Service.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "TaskScheduler.API.Service.csproj" -c Release -o /app/publish
RUN ls /app/publish
