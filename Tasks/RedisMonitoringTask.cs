﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using StackExchange.Redis;
using TaskScheduler.API.Service.Services;

namespace TaskScheduler.API.Service.Tasks
{
    public class RedisMonitoringTask : IHostedService
    {
        private readonly string _channelName;
        
        private readonly IConnectionMultiplexer _connectionMultiplexer;

        private readonly List<IRedisNotifier> _redisNotifiers;

        public RedisMonitoringTask(IConnectionMultiplexer connectionMultiplexer, List<IRedisNotifier> redisNotifiers)
        {
            _channelName = "__keyspace@0__:*";
            
            _connectionMultiplexer = connectionMultiplexer ?? throw new ArgumentNullException(nameof(connectionMultiplexer));

            _redisNotifiers = redisNotifiers ?? throw new ArgumentNullException(nameof(redisNotifiers));
        }
        
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var subscriber = _connectionMultiplexer.GetSubscriber();

            await subscriber.SubscribeAsync(_channelName, (channel, notificationType) =>
            {
                var key = GetKey(channel);
                
                Task.WaitAll(
                    _redisNotifiers.Select(
                        notifier => notifier.NotifyAsync(
                            key, 
                            RedisNotificationTypesExtensions.Get(notificationType)))
                        .ToArray());
            });
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        
        private string? GetKey(string? channel)
        {
            if (channel == null) return channel;
            
            var index = channel.IndexOf(':');
            if (index >= 0 && index < channel.Length - 1)
                return channel[(index + 1)..];

            return channel;
        }
        
        
    }
}