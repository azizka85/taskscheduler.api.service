using System;
using System.Collections.Generic;
using System.IO.Compression;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using TaskScheduler.API.Service.GrpcServices;
using TaskScheduler.API.Service.Services;
using TaskScheduler.API.Service.Tasks;

namespace TaskScheduler.API.Service
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            var redisConfiguration = GetRedisConfiguration("Redis");

            services.AddCors();
            
            services
                .AddSingleton<IConnectionMultiplexer>(_ => ConnectionMultiplexer.Connect(redisConfiguration))
                .AddSingleton(options => new TaskStreamsManager(
                    options.GetRequiredService<IConnectionMultiplexer>(),
                    options.GetRequiredService<ILogger<TaskStreamsManager>>()))
                .AddSingleton(options => new TasksService(
                    options.GetRequiredService<IConnectionMultiplexer>()));

            services
                .AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                });
            
            services.AddGrpc(options =>
            {
                options.ResponseCompressionAlgorithm = "gzip";
                options.ResponseCompressionLevel = CompressionLevel.Optimal;
            });

            services
                .AddHostedService(options => new RedisMonitoringTask(
                options.GetRequiredService<IConnectionMultiplexer>(),
                new List<IRedisNotifier>
                {
                    options.GetRequiredService<TaskStreamsManager>()
                }));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(builder =>
            {
                builder
                    .WithOrigins(_configuration.GetSection("Cors:Uri").Get<string[]>())
                    .WithExposedHeaders("X-Timestamp")
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });
            
            app.UseRouting();
            
            app.UseGrpcWeb(
                new GrpcWebOptions() { DefaultEnabled = true });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<TaskSchedulerService>();
            });
        }
        
        private ConfigurationOptions GetRedisConfiguration(string configSectionName)
        {
            var configSection = _configuration.GetSection(configSectionName);

            var redisConfiguration = ConfigurationOptions.Parse(configSection["ServerUri"]);
            configSection.Bind(redisConfiguration);

            return redisConfiguration;
        }
    }
}